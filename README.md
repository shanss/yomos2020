
<!-- badges: start -->

[![pipeline status](https://gitlab.gwdg.de/shanss/yomos2020/badges/master/pipeline.svg)](https://gitlab.gwdg.de/shanss/yomos2020/-/commits/master) [![coverage report](https://gitlab.gwdg.de/shanss/yomos2020/badges/master/coverage.svg)](https://gitlab.gwdg.de/shanss/yomos2020/-/commits/master)

<!-- badges: end -->
# YoMos2020

This repository is an example of how to set up a research compendium using the R package structure as a blueprint. It includes unit test and a simple continous integration pipeline using GitHub Actions.

This repository was created for a workshop at the virtual YoMos meeting 2020.

# You can find the presentation and step by step instructions on how to set up your own project [here](https://selinazitrone.github.io/YoMos2020/index.html)

References
==========

Marwick, Ben, Carl Boettiger, and Lincoln Mullen. 2018. “Packaging Data
Analytical Work Reproducibly Using R (and Friends).” *The American
Statistician* 72 (1): 80–88.
<https://doi.org/10.1080/00031305.2017.1375986>.
